# Solutions

```scala
//package demo

object Hello extends App {

  trait Animal { }

  case class Dog(val name: String) extends Animal
  case class Cat(val name: String) extends Animal
  case class Bird(val name: String) extends Animal
  case class Tiger(val name: String) extends Animal

  val sylvestre = Cat("Sylvestre")
  val titi = Bird("Titi")
  val lilly = Tiger("Lilly")

  def check(animal: Animal): Unit = { // Unit is a kind of void
    animal match {
      case Cat(name) => println(s"$name is a 🐱")
      case Dog(name) => println(s"$name is a 🐶")
      case Bird(name) => println(s"$name is a 🐦")
      case _ => println("???")
    }
  }

  //check(sylvestre)
  //check(titi)
  //check(lilly)

}

Hello.main(null)
```