object animals {

  class Animal(val name: String) {
    def hello() = { println(s"Hello I'm ${name}") }
  }

  // TODO: 👋 create a dog with a wouaf method
  class Dog(name: String) extends Animal(name) {
    def wouaf() = {
      println(s"Wouaf I'm ${name}")
    }
  }

}

import animals._

object Hello extends App {

  val wolf = new Dog("wolf")
  wolf.hello 
  wolf.wouaf 

}

Hello.main(null)

