object people {

  class Human(val name: String = "John Doe") {  
    def hello() = { print(s"Hello I'm $name") }
    def say(message: String) { println(s"Hello I'm $name, $message") }
  }

  //* --- companion object ---
  // ! no static class in Scala
  object Human {
    val message = "we are humans in the 🌍"
    def create(something: String) = new Human(something) 
  }
}

import people._

object Hello extends App {

  // TODO: display message of Human
  println(Human.message)

  // TODO: create a human named Bob
  val bob = Human.create("Bob")

  bob.say("👋 Hi")

}

Hello.main(null)