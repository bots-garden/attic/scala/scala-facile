object Hello extends App {

  val check = (value: Any) => value match {
    case s: String => s"this is a string: $s"
    case i: Int => s"this is an int: $i"
    case _ => "😡 Unknown"
  }

  println(check(42))
  println(check(34.5))
  println(check("Hello"))

}

Hello.main(null)
