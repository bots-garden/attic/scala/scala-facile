# Solutions

```scala
import $ivy.`io.vertx::vertx-web-scala:3.5.2`
//* libraryDependencies += "io.vertx" %% "vertx-web-scala" % "3.5.2"

import io.vertx.core.json.JsonObject
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.Router

import scala.util.{Try, Failure, Success}

object Hello extends App {
  val vertx = Vertx.vertx()
  val server = vertx.createHttpServer()
  val router = Router.router(vertx)

  val httpPort = sys.env.getOrElse("PORT", "9090").toInt

  router.get("/hello").handler(context => 
    context
      .response()
      .putHeader("content-type", "text/html;charset=UTF-8")
      .end("<h1>Hello 🌍</h1>")
  )

  router.get("/hey").handler(context => 
    context
      .response()
      .putHeader("content-type", "application/json;charset=UTF-8")
      .end(new JsonObject().put("message", "👋 hey!").encodePrettily())                            
  )

  router.get("/divide/:a/:b").handler(context => {
    val result = Try(
      context.request.getParam("a").get.toInt / 
      context.request.getParam("b").get.toInt
    ) match {
      case Failure(err) => new JsonObject().put("error", "🙀 oups")
      case Success(res) => new JsonObject().put("result", res)
    }
    context
      .response()
      .putHeader("content-type", "application/json;charset=UTF-8")
      .end(result.encodePrettily()) 

  })

  println(s"🌍 Listening on $httpPort  - Enjoy 😄")
  server.requestHandler(router.accept).listen(httpPort)

}

Hello.main(null)

```