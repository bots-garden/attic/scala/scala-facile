// package demo

import $ivy.`io.vertx::vertx-web-scala:3.5.2`

import io.vertx.core.json.JsonObject
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.Router

import scala.util.{Try, Failure, Success}

import io.vertx.scala.ext.web.RoutingContext

object magic {
	implicit class RoutingContextImprovements(val rc: RoutingContext) {
	  def json(j: JsonObject): Unit = { 
	    rc.response()
	      .putHeader("content-type", "application/json;charset=UTF-8")
	      .end(j.encodePrettily())
	  }

	  def html(c: String): Unit = { 
	    rc.response()
	      .putHeader("content-type", "text/html;charset=UTF-8")
	      .end(c)
	  }

    def intParam(paramName: String): Int = {
      Try(rc.request.getParam(paramName).get.toInt) match {
        case Failure(err) => 0
        case Success(res) => res
      }
    }
	}
}

import magic._

object Hello extends App {

  val vertx = Vertx.vertx()
  val server = vertx.createHttpServer()
  val router = Router.router(vertx)

  val httpPort = sys.env.getOrElse("PORT", "9090").toInt

  router.get("/hello").handler(context => 
    context.html("<h1>👋 Hello 🌍 😝!</h1>")
  )

  router.get("/hey").handler(context => 
    context.json(new JsonObject().put("message", "👋 hey!"))                            
  )

  router.get("/divide/:a/:b").handler(context =>
    context.json(
      Try(context.intParam("a") / context.intParam("b")) match {
        case Failure(err) => new JsonObject().put("error", "🙀 oups")
        case Success(res) => new JsonObject().put("result", res)
      }
    )
  )

  println(s"🌍 Listening on $httpPort  - Enjoy 😄")
  server.requestHandler(router.accept).listen(httpPort)

}

Hello.main(null)