

// ! when
val model = Resources.load("en-token.bin")
when(model) {
  is Failure -> println(model.exception.message)
  is Success -> {
    println(model.value) 
  }
}

// ! when and let
Similarity(text, "hey @bob").sentences.let {
  when(it) {
    is Failure -> println(it.exception.message)
    is Success -> {
      it.value.forEach { sentence ->
        println(sentence)
      }
    }
  }
}

// * https://arrow-kt.io/